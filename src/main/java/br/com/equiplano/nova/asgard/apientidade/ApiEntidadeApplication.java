package br.com.equiplano.nova.asgard.apientidade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiEntidadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiEntidadeApplication.class, args);
	}

}
