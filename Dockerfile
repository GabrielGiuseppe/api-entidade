FROM adoptopenjdk/openjdk11
ADD target/api-entidade-0.0.1-SNAPSHOT.jar api-entidade.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "api-entidade.jar"]
